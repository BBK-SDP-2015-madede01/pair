
import java.util.List;
import java.util.Random;

/**
 * An instance represents a Solver that intelligently determines
 * Moves using the Minimax algorithm.
 */
public class AI implements Solver {

    // the current player
    private Player player;
    
    // Weigthing board column factor and win/lose at both end of arrays
    private static int[] weighting = new int[Board.NUM_COLS];

    /**
     * The depth of the search in the game space when evaluating moves.
     */
    private int depth;
    
    /*
    Initialise static variables for position scoring.
    Currently : -1,0,1
    Will be : -
    */
    static {
        int peekPosition = Math.abs(Board.NUM_COLS / 2);
        int weight = 0;
        
        for(int index = 0; index < Board.NUM_COLS; index++) {
            if(index < peekPosition) {
                weighting[index] = weight++;
            } else {
                weighting[index] = weight--;
            }
        }
    }

    /**
     * Constructor: an instance with player p who searches to depth d
     * when searching the game space for moves.
     */
    public AI(Player p, int d) {
        player = p;
        depth = d;
    }

    /**
     * See Solver.getMoves for the specification.
     */
    @Override
    public Move[] getMoves(Board b) {
        State root = new State(player, new Board(b), null);
        createGameTree(root, depth);

        minimax(root);
        Move [] move = b.getPossibleMoves(player);
        return move;
    }

    /**
     * Generate the game tree with root s of depth d.
     * The game tree's nodes are State objects that represent the state of a game
     * and whose children are all possible States that can result from the next move.
     * <p/>
     * NOTE: this method runs in exponential time with respect to d.
     * With d around 5 or 6, it is extremely slow and will start to take a very
     * long time to run.
     * <p/>
     * Note: If s has a winner (four in a row), it should be a leaf.
     */
    public static void createGameTree(State s, int d) {
        // Note: This method must be recursive, recurse on d,
        // which should get smaller with each recursive call

        // TODO

        // Create exit strategy form recursion
        if(d <= 0) {
            return;
        }

        boolean hasChildren = (s.getChildren().length > 0);
        
        if(hasChildren)
        {
            // Loop through Children and call initialiseChildren
            for(State child : s.getChildren()) {
                child.initializeChildren();
                createGameTree(child, --d);
            }
        }
        else
        {
            // Call initialiseChildren from root state
            s.initializeChildren();
            createGameTree(s, --d);
        }
    }

    /**
     * Call minimax in ai with state s.
     */
    public static void minimax(AI ai, State s) {
        ai.minimax(s);
    }
    
    /**
     * State s is a node of a game tree (i.e. the current State of the game).
     * Use the Minimax algorithm to assign a numerical value to each State of the
     * tree rooted at s, indicating how desirable that java.State is to this player.
     */
    public void minimax(State s) {
        // TODO
        /* 
        Get to the end of the tree... place a value.
        The subsequent states are possibilities after the current chosen point.
        
        Rules
        1. If you have a scenario where player wins score positive number.
        2. If you have a scenario where opponents wins score negative number.
        3. If no win/draw, setValue of state to zero.
        */
        for(State childstate : s.getChildren()) {
            minimax(childstate);
            /*
            Recursed to the ends of the GameTree and start setting the values.
            If childstate has children you set value based on children and 
            */
            if(childstate.getChildren().length > 0)
            {
                /* 
                We have children so we set values dependant on 
                children and highest/lowest values
                */
                if(childstate.getPlayer() == this.player) {
                    int max = StateScore.LOSE.getValue();  
                    State toReturn = null;
                    for(State state : childstate.getChildren()) {
                        if(state.getValue() > max) 
                        { 
                            max = state.getValue();
                            toReturn = state; 
                        }
                    }
                }
                else {
                    int min = StateScore.WIN.getValue();
                    State toReturn = null;
                    for(State state : childstate.getChildren()) {
                        if(state.getValue() < min)
                        { 
                            min = state.getValue();
                            toReturn = state; 
                        }
                    }
                }
            }
            else
            {
                /*
                At this point we have no children so lets start valuing the 
                options.
                */
                
                if(childstate.getBoard().hasConnectFour()==this.player) {
                    System.out.println("State being graded as a WIN");
                    System.out.println(childstate);
                    childstate.setValue(StateScore.WIN.getValue());
                }
                else if(childstate.getBoard().hasConnectFour()==this.player.opponent()) {
                    System.out.println("State being graded as a LOSE");
                    System.out.println(childstate);
                    childstate.setValue(StateScore.LOSE.getValue());
                }
                else
                {
                /* 
                If we have a tie situation, 
                grade the position from last moves column
                */
                Move move = childstate.getLastMove();
                int column = move.getColumn();
                
                
                System.out.println("State being graded as a TIE");
                System.out.println(childstate);
                childstate.setValue(StateScore.TIE.getValue() + weighting[column]);
                }
            }
        }
    }

    /**
     * Evaluate the desirability of Board b for this player
     * Precondition: b is a leaf node of the game tree (because that is most
     * effective when looking several moves into the future).
     */
    public int evaluateBoard(Board b) {
        Player winner = b.hasConnectFour();
        int value = 0;
        if (winner == null) {
            // Store in sum the value of board b. 
            List<Player[]> locs = b.winLocations();
            for (Player[] loc : locs) {
                     for (Player p : loc) {
                    value += (p == player ? 1 : p != null ? -1 : 0);
                }
            }
        } else {
            // There is a winner
            int numEmpty = 0;
            for (int r = 0; r < Board.NUM_ROWS; r = r + 1) {
                for (int c = 0; c < Board.NUM_COLS; c = c + 1) {
                    if (b.getTile(r, c) == null) numEmpty += 1;
                }
            }
            value = (winner == player ? 1 : -1) * 10000 * numEmpty;
        }
        return value;
    }
    
    private enum StateScore {
        LOSE(-1), TIE(0), WIN(1);//WIN(Board.NUM_COLS/2);
        private final int value;
        private StateScore(int value) { this.value = value; }
        public int getValue() { return value; }
        public static StateScore fromValue(int value) {
            for(StateScore score : values()) {
                if(score.getValue() == value) {
                    return score;
                }
            }
            return TIE;
        }
    }
    
}
